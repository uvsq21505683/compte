import static org.junit.Assert.*;

import org.junit.Test;


public class test {

	@Test
	public void test() {
int tmp=40;
compte a=new compte();
a.solde(100);
//assertEquals(a.getSolde(),100);
assertTrue(a.getSolde()>0); 
assertFalse(a.getSolde()<0); 
	}
	
	
	@Test 
	public void test_ajout_negatif(){
		
		compte a=new compte();
		a.solde(100);
		a.credit(-10);
		assertEquals(a.getSolde(),100);
	}
	

	@Test 
	public void test_ajout_positif(){
		
		compte a=new compte();
		a.solde(100);
		a.credit(100);
		assertEquals(a.getSolde(),200);
	}	
	
	@Test 
	public void test_jamais_decouvert(){
		
		compte a=new compte(-100);
		assertEquals(a.getSolde(),0);
	}
	
	@Test
	public void test_debit_sup(){
		
		compte a=new compte(100);
		a.debit(200);
		assertEquals(a.getSolde(),100); 
	}	
	
	@Test
	public void test_vir_sup(){
		
		compte a=new compte(100);
		compte b=new compte(0);
		a.virement(b,50);
		assertEquals(a.getSolde(),50); 
	}
}
